Generate the key for common.java

![](https://gitlab.com/peter-example/java/azure-storage-java-example/-/raw/master/generate%20sas%20key.png?ref_type=heads)

Generate endpoint

![](https://gitlab.com/peter-example/java/azure-storage-java-example/-/raw/master/generate%20endpoint.png?ref_type=heads)