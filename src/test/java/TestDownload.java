
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobContainerClientBuilder;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestDownload {

	@Test
	public void testDownload() {
		BlobContainerClient containerClient = new BlobContainerClientBuilder().endpoint(Common.endpoint).buildClient();
		BlobClient blobClient = containerClient.getBlobClient("hkps.png");
		blobClient.downloadToFile("hkps.png");
	}

}
