
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobContainerClientBuilder;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestListFiles {

	@Test
	public void testDownload() {
		BlobContainerClient containerClient = new BlobContainerClientBuilder().endpoint(Common.endpoint).buildClient();
		containerClient.listBlobs()
				.forEach(blob -> System.out.printf("Name: %s=%d%n", blob.getName(), blob.getProperties().getContentLength()));
	}

}
