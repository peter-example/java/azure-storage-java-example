
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobContainerClientBuilder;
import com.azure.storage.blob.models.PageRange;
import com.azure.storage.blob.specialized.PageBlobClient;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestPartialUpload {

	@Test
	public void testUpload() throws FileNotFoundException {
		BlobContainerClient containerClient = new BlobContainerClientBuilder().endpoint(Common.endpoint).buildClient();
		Date d1 = new Date();
		PageBlobClient pageBlobClient = containerClient.getBlobClient("1.txt").getPageBlobClient();
		pageBlobClient.create(2048);
		InputStream targetStream = new ByteArrayInputStream(StringUtils.rightPad("Peter123",512).getBytes());
		pageBlobClient.uploadPages(new PageRange().setStart(512).setEnd(512+511), targetStream);
		Date d2 = new Date();
		System.out.println((d2.getTime()-d1.getTime())/1000);
	}

}
