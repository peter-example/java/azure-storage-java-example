
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobContainerClientBuilder;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestUpload {

	@Test
	public void testDownload() {
		BlobContainerClient containerClient = new BlobContainerClientBuilder().endpoint(Common.endpoint).buildClient();
//		BlobClient blobClient = containerClient.getBlobClient("Azure Boot Camp黎啦.png");
//		blobClient.uploadFromFile("Azure Boot Camp黎啦.png");

		Date d1 = new Date();
		BlobClient blobClient = containerClient.getBlobClient("Java 52堂 (final).mp4");
		blobClient.uploadFromFile("/Users/peter/Desktop/Java 52堂 (final).mp4");
//		BlobHttpHeaders headers = new BlobHttpHeaders();
//		headers.setContentType("video/mp4");
//		blobClient.setHttpHeaders(headers);

		Date d2 = new Date();
		System.out.println((d2.getTime()-d1.getTime())/1000);
	}

}
